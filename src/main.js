import Vue from "vue";
import App from "./App.vue";
import axios from "axios";
import domtoimage from "dom-to-image-more";

Vue.config.productionTip = false;

Vue.prototype.$http = axios;

new Vue({
  render: (h) => h(App, domtoimage),
}).$mount("#app");
